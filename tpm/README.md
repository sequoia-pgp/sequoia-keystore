A TPM backend for Sequoia's private key store.

The `sequoia-keystore` crate implements a server that manages secret
keys.  Secret key material can be stored in files, on hardware devices
like smartcards, or accessed via the network.  `sequoia-keystore`
doesn't implement these access methods.  This is taken care of by
various backends.

This crate includes a backend that exposes the secret keys managed by
a TPM.

# Using your TPM

In TPM terminology, keys are named by handles.  To list any persistent
keys, use `tpm2_getcap` (from the `tpm2-tools` package in Debian) do
the follow as root or add yourself to the appropriate group (`tss` on
Debian, or examine `/dev/tpmrm0`):

```text
$ tpm2_getcap handles-persistent
- 0x81000001
- 0x81000002
- 0x81010001
```

To create a key on the TPM, you can use `create-key` from
[`sequoia-tpm`], as well the templates [`rsa2k-dec.yml`] and
[`rsa2k-sign.yml`] from this project.

  [`sequoia-tpm`]: https://gitlab.com/sequoia-pgp/sequoia-tpm
  [`rsa2k-dec.yml`]: https://gitlab.com/sequoia-pgp/sequoia-keystore/-/blob/main/tpm/examples/rsa2k-dec.yml
  [`rsa2k-sign.yml`]: https://gitlab.com/sequoia-pgp/sequoia-keystore/-/blob/main/tpm/examples/rsa2k-sign.yml

```text
$ /tmp/sequoia-tpm/target/debug/create-key --file .../sequoia-keystore/tpm/examples/rsa2k-dec.yml
Generated key handle = 0x40418488
$ /tmp/sequoia-tpm/target/debug/create-key --file .../sequoia-keystore/tpm/examples/rsa2k-sign.yml
Generated key handle = 0x40418488
```

The handles that are shown are the temporary handles.  The persistent
handles are set in the `yaml` files.

In my case, we can see that two persistent handles have been added:

```text
$ tpm2_getcap handles-persistent
- 0x81000001
- 0x81000002
- 0x81000018
- 0x81000027
- 0x81010001
```

To delete a key do:

```
$ tpm2_evictcontrol --object-context=0x81000027
persistent-handle: 0x81000027
action: evicted
```

To get the public key, we use `get-key` from `sequoia-tpm`.  For example:

```text
$ /tmp/sequoia-tpm/target/debug/get-key --file .../sequoia-keystore/tpm/examples/rsa2k-sign.yml
---
spec:
  provider:
    tpm:
      tcti: "device:/dev/tpmrm0"
      handle: 2164260903
      parent: ~
      private: ~
      unique:
        RSA:
          bytes: d800779885492a9e3712880bbaefe348365eff39bb0fabf92262e01b3d96efc723ab69eee2e32a0ca4687e0562182ff4343cbd3ade5091bcb4e220c02cc741c39765e5a38490c69dda7bfad8ae0b1b521ec47f37fafb3b0570618155441ed516d6b1e0c09a97065800a54d9837fc4a9ef3b9d568b4ef69ce4db36097c31b35775e743c43fef19591f0cf444511f561eb2cecc3c5f3c18912ea060500026c338548682ce894313e54846e0e4b3e421250c9a844e67ce470e909f4b7a1f065b83c391164528fd12e0a4581340500e2c6863342304b5eb56df09e7083c0a826853c3ecf436294e2c06ad7090e74ba6f78e7450c6002d80ee15e7ea1f364bc39f071
      wrapped: ~
      policy: ""
  algo:
    RSA:
      bits: 2048
      exponent: ~
  private: ~
  capabilities:
    - sign
  auth: "123"
```

To add the key to the keystore, copy the descriptions to the
home directory for the keystore's tpm:

```text
$ mkdir -p ~/.local/share/sequoia/keystore/tpm
$ cp .../sequoia-keystore/tpm/examples/rsa2k-{dec,sign}.yml ~/.local/share/sequoia/keystore/tpm
```

`sq` is not yet able to use the keys.  For that we have to add OpenPGP
metadata to the keys, and then add the keys to an OpenPGP certificate.
For the first part, we create a "null" OpenPGP certificate using the
`get-openpgp-key` utility from `sequoia-tpm`.  A null OpenPGP
certificate is just a certificate with a single packet: a primary key,
whose creation time is set to the unix epoch.  It doesn't have any
signatures, so it is not a valid certificate, but `sq` can work with
it.


```text
$ /tmp/sequoia-tpm/target/debug/get-openpgp-key --file ~/.local/share/sequoia/keystore/tpm/rsa2k-sign.yml >/tmp/sign.pgp
$ sq inspect /tmp/sign.pgp
/tmp/sign.pgp: Revocation Certificate.

    Fingerprint: E5637B7762959BB59ACB8E9426F9CAC1FA7C375F
                 Invalid: No binding signature at time 2024-08-29T09:29:30Z
Public-key algo: RSA
Public-key size: 2048 bits
  Creation time: 1970-01-01 00:00:00 UTC

$ /tmp/sequoia-tpm/debug/get-openpgp-key --file ~/.local/share/sequoia/keystore/tpm/rsa2k-dec.yml >/tmp/dec.pgp
$ sq inspect /tmp/dec.pgp
/tmp/dec.pgp: Revocation Certificate.

    Fingerprint: B321BA8F650CB16443E06826DBFA98A78CF6562F
                 Invalid: No binding signature at time 2024-08-29T09:29:39Z
Public-key algo: RSA
Public-key size: 2048 bits
  Creation time: 1970-01-01 00:00:00 UTC
```

We import the null certificates into `sq`, and the keys now appear:

```text
$ sq cert import /tmp/sign.pgp /tmp/dec.pgp
Imported E5637B7762959BB59ACB8E9426F9CAC1FA7C375F, <unknown>
Imported B321BA8F650CB16443E06826DBFA98A78CF6562F, <unknown>
Imported 2 new certificates, updated 0 certificates, 0 certificates unchanged, 0 errors.
$ sq key list
 - softkeys
 - tpm
   - device:/dev/tpmrm0
     - B321BA8F650CB16443E06826DBFA98A78CF6562F <unknown> (available, unlocked, for decryption)
     - E5637B7762959BB59ACB8E9426F9CAC1FA7C375F <unknown> (available, unlocked, for signing)
...
```

For the keys to be useful, we need to add them to a certificate.  In
the following example, we create a new certification-only certificate.
We don't add any subkeys, since we want to use the decryption and
signing keys on the TPM.  You could use an existing certificate.

```text
$ sq key generate --email test@example.org --cannot-encrypt --cannot-sign --cannot-authenticate
Please enter the password to protect key (press enter to not use a password): 
                                                  Please repeat the password: 
Transferable Secret Key.

    Fingerprint: 5D4FE5AC4549548192AD2AFBFE61C03A3EB58C7B
Public-key algo: EdDSA
Public-key size: 256 bits
     Secret key: Unencrypted
  Creation time: 2024-08-29 09:16:26 UTC
Expiration time: 2027-08-30 02:42:47 UTC (creation time + 2years 11months 30days 9h 16m 45s)
      Key flags: certification

         UserID: <test@example.org>
$ sq pki link add --ca \* 5D4FE5AC4549548192AD2AFBFE61C03A3EB58C7B --all
Linking 5D4FE5AC4549548192AD2AFBFE61C03A3EB58C7B and "<test@example.org>".
$ sq key adopt --can-encrypt=universal  --expiration 2030-01-01 --cert 5D4FE5AC4549548192AD2AFBFE61C03A3EB58C7B --key B321BA8F650CB16443E06826DBFA98A78CF6562F
Hint: Imported updated cert into the cert store.  To make the update effective, it has to be
      published so that others can find it, for example using:

  sq network keyserver publish --cert FE61C03A3EB58C7B
$ sq key adopt --cert 5D4FE5AC4549548192AD2AFBFE61C03A3EB58C7B --key E5637B7762959BB59ACB8E9426F9CAC1FA7C375F --can-sign  --expiration 2030-01-01
Hint: Imported updated cert into the cert store.  To make the update effective, it has to be
      published so that others can find it, for example using:

  sq network keyserver publish --cert FE61C03A3EB58C7B
$ sq inspect --cert 5D4FE5AC4549548192AD2AFBFE61C03A3EB58C7BOpenPGP Certificate.

    Fingerprint: 5D4FE5AC4549548192AD2AFBFE61C03A3EB58C7B
Public-key algo: EdDSA
Public-key size: 256 bits
  Creation time: 2024-08-29 09:16:26 UTC
Expiration time: 2027-08-30 02:42:47 UTC (creation time + 2years 11months 30days 9h 16m 45s)
      Key flags: certification

         Subkey: B321BA8F650CB16443E06826DBFA98A78CF6562F
Public-key algo: RSA
Public-key size: 2048 bits
  Creation time: 1970-01-01 00:00:00 UTC
Expiration time: 2030-01-01 00:00:00 UTC (creation time + 60years)
      Key flags: transport encryption, data-at-rest encryption

         Subkey: E5637B7762959BB59ACB8E9426F9CAC1FA7C375F
Public-key algo: RSA
Public-key size: 2048 bits
  Creation time: 1970-01-01 00:00:00 UTC
Expiration time: 2030-01-01 00:00:00 UTC (creation time + 60years)
      Key flags: signing, transport encryption, data-at-rest encryption

         UserID: <test@example.org>

```

Finally, we can create signatures and decrypt messages using our
OpenPGP certificate:

```text
$ echo a message | sq sign --signer-key 5D4FE5AC4549548192AD2AFBFE61C03A3EB58C7B | sq verify
Authenticating 5D4FE5AC4549548192AD2AFBFE61C03A3EB58C7B ("<test@example.org>") using the web of
trust:
  Fully authenticated (120 of 120) 5D4FE5AC4549548192AD2AFBFE61C03A3EB58C7B, <test@example.org>
    ◯ DBDE6D01BBE73919A2645E60CB2308D04C8D0A6A ("Local Trust Root")
    │   certified the following binding on 2024-08-29 as a fully trusted meta-introducer (depth:
    │   unconstrained)
    └ 5D4FE5AC4549548192AD2AFBFE61C03A3EB58C7B "<test@example.org>"

  Good signature from 26F9CAC1FA7C375F ("<test@example.org>")

a message
1 good signature.
$ echo a message | sq encrypt --recipient-cert 5D4FE5AC4549548192AD2AFBFE61C03A3EB58C7B | sq decrypt
Encrypted using AES-256
a message
```
