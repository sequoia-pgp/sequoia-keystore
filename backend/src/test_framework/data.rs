//! Test data.
//!
//! Includes the test data from `tests/data` in a structured way.
use std::collections::BTreeMap;

/// Returns the content of the given file below `tests/data`.
pub fn file(name: &str) -> &'static [u8] {
    try_file(name).unwrap_or_else(|| panic!("No such file {:?}", name))
}

/// Returns the content of the given file below `tests/data`, fallible
/// version.
pub fn try_file(name: &str) -> Option<&'static [u8]> {
    use std::sync::OnceLock;

    static FILES: OnceLock<BTreeMap<&'static str, &'static [u8]>>
        = OnceLock::new();
    FILES.get_or_init(|| {
        let mut m: BTreeMap<&'static str, &'static [u8]> =
            Default::default();

        macro_rules! add {
            ( $key: expr, $path: expr ) => {
                m.insert($key, include_bytes!($path))
            }
        }
        include!(concat!(env!("OUT_DIR"), "/tests.index.rs.inc"));

        // Sanity checks.
        assert!(m.contains_key("keys/no-password.asc"));
        assert!(m.contains_key("messages/no-password.asc"));
        m
    }).get(name).cloned()
}

/// Returns the content of the given file below `tests/data/keys`.
pub fn key(name: &str) -> &'static [u8] {
    try_key(name).unwrap_or_else(|| panic!("No such key {:?}", name))
}

/// Returns the content of the given file below `tests/data/keys`,
/// fallible version.
pub fn try_key(name: &str) -> Option<&'static [u8]> {
    try_file(&format!("keys/{}", name))
}

/// Returns the content of the given file below `tests/data/messages`.
pub fn message(name: &str) -> &'static [u8] {
    try_message(name).unwrap_or_else(|| panic!("No such message {:?}", name))
}

/// Returns the content of the given file below `tests/data/messages`,
/// fallible version.
pub fn try_message(name: &str) -> Option<&'static [u8]> {
    try_file(&format!("messages/{}", name))
}
