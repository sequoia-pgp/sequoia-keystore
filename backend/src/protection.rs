/// How secret key material is protected.
#[derive(Debug)]
pub enum Protection {
    // keystore_protocol.capnp Protection:

    /// The secret key material is unlocked.
    Unlocked,

    /// The key store is not able to determine if the secret key
    /// material is protected.
    ///
    /// It is, however, safe to try a secret key operation (e.g., the
    /// retry counter will not be decremented).  Trying an operation
    /// may trigger an external event, like a system pin entry dialog.
    UnknownProtection(Option<String>),

    /// The secret key material is protected by a password.  It can
    /// be unlocked using the unlock interface.
    ///
    /// The string is an optional hint for the user.
    Password(Option<String>),

    /// The secret key material is protected, and can only be unlocked
    /// using an external terminal.
    ///
    /// The string is an optional hint for the user.
    ///
    /// Note: some devices don't provide a mechanism to determine if
    /// the secret key material is currently locked.  For instance,
    /// some smart cards can be configured to require the user to
    /// enter a pin on an external keypad before their first use, but
    /// not require it as long as the smart card remains attached to
    /// the host, and also not provide a mechanism for the host to
    /// determine the current policy.  Such devices should still
    /// report `Protection::ExternalPassword`, and should phrase the
    /// hint appropriately.
    ExternalPassword(Option<String>),

    /// The secret key material is protected, and can only be unlocked
    /// if the user touches the device.
    ///
    /// The string is an optional hint for the user.
    ExternalTouch(Option<String>),

    /// The secret key material is protected, and can only be unlocked
    /// externally.
    ///
    /// The string is an optional hint for the user, e.g., "Please connect
    /// to the VPN."
    ExternalOther(Option<String>),
}
