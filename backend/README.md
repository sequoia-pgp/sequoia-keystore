The `sequoia-keystore` crate implements a server that manages secret
key material.  Secret key material can be stored in files, on hardware
devices like smartcards, or accessed via the network.
`sequoia-keystore` doesn't implement these access methods.  This is
taken care of by various backends.  The backends implement a common
interface, which is defined by this crate.
