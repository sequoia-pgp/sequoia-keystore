Sequoia's keystore server.

This program is a simple wrapper around the [`sequoia-keystore`]
library, which runs as a server.  Normally programs like `sq` will
automatically start servers on demand.  This crate's binary,
`sequoia-keystore`, should be installed in `/usr/lib/sequoia` so that
`sq` and other programs can find it.

Note: by [default] programs such as `sq` look for the executables in
`/usr/local/lib/sequoia`, but this can be tweaked by setting the
`PREFIX` environment variable while building `sq`.

  [default]: https://gitlab.com/sequoia-pgp/sequoia/-/blob/main/ipc/src/core.rs?ref_type=heads#L85

Servers can also be started explicitly by just running the binary.

If the server can't be started, the server is also usually embedded in
the programs, and an in-process server is used instead.  The
in-process server has several disadvantages, though:

  - Secret key material is in the same process, which makes the
    program more vulnerable to [Heartbleed]-style attacks.

  - It may be harder to use resources like smart cards from multiple
    process.

  - Passwords will only be cached locally.

  [Heartbleed]: https://heartbleed.com/

In some cases, the in-process server is preferable, like early in the
boot process when starting processes is hard, or the file system is
not completely setup.
