pub use sequoia_keystore_backend::ImportStatus;

use crate::Result;
use crate::keystore_protocol_capnp::keystore;

impl TryFrom<keystore::import_status::Reader<'_>> for ImportStatus {
    type Error = anyhow::Error;

    fn try_from(reader: keystore::import_status::Reader<'_>) -> Result<Self> {
        use keystore::import_status::Which;

        match reader.which()? {
            Which::New(()) => Ok(ImportStatus::New),
            Which::Updated(()) => Ok(ImportStatus::Updated),
            Which::Unchanged(()) => Ok(ImportStatus::Unchanged),
        }
    }
}

impl keystore::import_status::Builder<'_> {
    /// Serialize ImportStatus.
    pub fn set_import_status(&mut self, import_status: ImportStatus) -> Result<()> {
        match import_status {
            ImportStatus::New => self.set_new(()),
            ImportStatus::Updated => self.set_updated(()),
            ImportStatus::Unchanged => self.set_unchanged(()),
        }

        Ok(())
    }
}
