A simple keystore with three keys (Alice, Bob, and Carol), which are
all unlocked.  Bob's certificate is expired (which shouldn't matter
for the purpose of decrypting).  Alice's and Bob's keys use ECC
whereas Carol's key uses RSA.

Some messages are also encrypted for a different key, Dave.  (It is
stored next to this file, but not imported into the `keystore`
directory.)  One message is encrypt just to dave.  Some of the
messages also include dave as a recipient.

Files are named after the recipients (in the order of the PKESKs),
e.g., `for-dave-alice.pgp`.  The content is the filename without the
extension plus a newline.

```text
$ sq inspect alice.pgp
    Fingerprint: AE81192975C08CA56C257BD3A29F4223DDD881B2
Public-key algo: EdDSA

         Subkey: E424FDC35BE93C90686A99E8D4D08F3C919C9ED7
      Key flags: authentication

         Subkey: 48DFA35094CE7D9EE0483EDB61779B413826EB98
      Key flags: signing

         Subkey: EBB7CD5ED3AF234A0AD15BDA1B19948D7A8424A4
      Key flags: transport encryption, data-at-rest encryption

         UserID: Alice <alice@example.org>

$ sq inspect bob.pgp
bob.pgp: Transferable Secret Key.

    Fingerprint: 9BD6F501FCB436970C1FE23F51B9EA96699DDBA5
  Creation time: 2022-09-26 21:06:22 UTC
Expiration time: 2022-09-26 23:06:22 UTC (creation time + PT7200S)
      Key flags: certification

         Subkey: 48DAD7FE5E50183B818C009B42B7FCE2D4D9B4B8
      Key flags: authentication

         Subkey: 54EFD9DB3DCFEDF69C3DC45FA08D6FA15CDB3736
      Key flags: signing

         Subkey: 92D7E5A2005676E7F2B350B11DF3BDD1D6072B44
      Key flags: transport encryption, data-at-rest encryption

         UserID: Bob <bob@example.org>

$ sq inspect carol.pgp
carol.pgp: Transferable Secret Key.

    Fingerprint: CD6C4799E67CC4A551B7F2F400431FF5B8A682CD
Public-key algo: RSA
Public-key size: 4096 bits

         Subkey: AF2620370975120E25A0469FAC13CAA7E282324B
      Key flags: transport encryption, data-at-rest encryption

         Subkey: 9CA852189BDB571874AEA5BC971033045BE0B34D
      Key flags: signing

         Subkey: D786023D2D8D4B302BA8896FAF8A2B1CD1B9561D
      Key flags: authentication

         UserID: Carol <carol@example.org>
```
