An openpgp card backend for Sequoia's private key store.

The `sequoia-keystore` crate implements a server that manages secret
keys.  Secret key material can be stored in files, on hardware devices
like smartcards, or accessed via the network.  `sequoia-keystore`
doesn't implement these access methods.  This is taken care of by
various backends.

This crate includes a backend that exposes the secret keys managed by
OpenPGP cards.  It uses PCSC in order to access the cards.  On Linux
distributions, this means that you'll need to install a PCSC service
like PC/CS Lite.  On Debian this is provided by the `pcscd` package.
