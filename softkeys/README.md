A soft key (in-memory key) backend for Sequoia's private key store.

The `sequoia-keystore` crate implements a server that manages secret
key material.  Secret key material can be stored in files, on hardware
devices like smartcards, or accessed via the network.
`sequoia-keystore` doesn't implement these access methods.  This is
taken care of by various backends.

This crate includes a backend that provides access to secret key
material stored in files.  These are called soft keys in contrast to
keys managed by a separate piece of hardware.

If the keystore is configured to use `~/.local/share/sequoia` as its
data directory, then the soft key backend uses
`~/.local/share/sequoia/keystore/softkeys`.  Specifically, it iterates
over the files in that directory, and looks for binary encoded or
ASCII-armor encoded OpenPGP Transferable Secret Keys in files ending with `.pgp`
or `.asc`; other files are silently ignored.

```
$ ls .local/share/sequoia/keystore/softkeys/
1234.pgp  1234.pgp.rev  alice.pgp  alice.pgp.rev  F44B66C85C9B7B02AF2D52FDEFF613897AD9CE21.pgp
```
