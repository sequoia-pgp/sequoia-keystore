# Sequoia's private Key Store

This project implements a private key store for Sequoia.  A private
key store mediates applications' access to private keys, and offers
three major advantages relative to every application accessing the
keys or HSMs directly:

  - A private key store is in a separate address space.  This means
    that private keys that are in memory are in a different address
    space from the application.  This was underlying cause of the
    [Heartbleed vulnerability].

    [Heartbleed vulnerability]: https://de.wikipedia.org/wiki/Heartbleed

  - A private key store can provide a uniform interface for accessing
    keys stored on different backends, e.g., an in-memory key, a key
    on a smart card, or a key on a remote computer, which is accessed
    via ssh.  This simplifies applications.

  - This architecture simplifies sharing private key material among
    multiple applications. Only the private key store needs to worry
    about managing the private key material, which improves
    security. And, when a user unlocks a key in one application, it is
    potentially unlocked in all applications, which improves
    usability.

Although the key store can run as a separate server, sometimes it is
useful to co-locate it.  This is useful to increase robustness, e.g.,
the key store is not running, and can't be started for some reason.
And, it allows the key store to be used in places where starting
processes is not easy or not desirable, like in an initrd.

The private key store uses a device-driver style architecture.  The
[sequoia-keystore-backend crate] defines a trait that different
backends implement.  Currently, [backends are added at compile time].

  [sequoia-keystore-backend crate]: https://gitlab.com/sequoia-pgp/sequoia-keystore/-/tree/main/backend
  [backends are added at compile time]: https://gitlab.com/sequoia-pgp/sequoia-keystore/-/blob/main/keystore/src/server/backend.rs

The [sequoia-keystore-softkeys] backend is an example of a backend.
It supports soft keys, i.e., in-memory keys.

  [sequoia-keystore-softkeys]: https://gitlab.com/sequoia-pgp/sequoia-keystore/-/tree/main/softkeys

## Packaging the Key Store

Programs that use the key store, like `sq`, work fine as-is because
they fall back to co-locating the key store server in their own
address space.  However, there are some benefits to having the key
store as a long-running backend server, such as password caching,
reduced startup overhead, and better hardware handling.

To that end, we have the crate `sequoia-keystore-server` which
produces a standalone key store program called `sequoia-keystore`.
This program should be installed as
`$PREFIX/lib/sequoia/sequoia-keystore`.  Then, programs using the key
store, like `sq`, should be built using `$PREFIX` set appropriately
(it defaults to `/usr/local`), and the package should depend on (or at
least suggest) the key store server package.

When installed in the correct location, and the client programs being
configured at build-time or run-time to find it, the server will be
started on demand.  To start it as part of the user's desktop session,
and terminate it at the end of it, you can use a service manager such
as systemd.  A systemd service file is provided for your convenience,
see `sequoia-keystore.service`.
